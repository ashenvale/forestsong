<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Entity\Factory;

use Ashenvale\Forestsong\Entity\Investment;
use Ashenvale\Forestsong\Entity\Stock;
use Ashenvale\Forestsong\ValueObject\Investment as ValueObject;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ValueObject\Exception\ViolationsException;

class InvestmentFactory
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator;
    }

    public function create(ValueObject $valueObject, int $userId, Stock $stock)
    {
        $violations = $this->validator->validate($valueObject, null, 'investment');
        if (\count($violations) > 0) {
            throw new ViolationsException($violations);
        }

        return new Investment(
            $userId,
            $stock,
            $valueObject->getQuantity(),
            $valueObject->getPrice(),
            $valueObject->getInvestedAt()
        );
    }
}
