<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Entity\Factory;

use Ashenvale\Forestsong\Entity\Stock;
use Ashenvale\Forestsong\ValueObject\Investment as ValueObject;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ValueObject\Exception\ViolationsException;

class StockFactory
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator;
    }

    public function create(ValueObject $valueObject)
    {
        $violations = $this->validator->validate($valueObject, null, 'stock');
        if (\count($violations) > 0) {
            throw new ViolationsException($violations);
        }

        return new Stock(
            $valueObject->getStockId(),
            $valueObject->getStockName(),
            $valueObject->getStockPinyin()
        );
    }
}
