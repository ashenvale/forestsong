<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Entity;

class Investment
{
    private $id;

    private $userId;

    private $stock;

    private $quantity;

    private $price;

    private $investedAt;

    public function __construct(
        int $userId,
        Stock $stock,
        int $quantity,
        float $price,
        \DateTime $investedAt
    ) {
        $this->userId = $userId;
        $this->stock = $stock;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->investedAt = $investedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getStock(): Stock
    {
        return $this->stock;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getInvestedAt(): \DateTime
    {
        return $this->investedAt;
    }
}
