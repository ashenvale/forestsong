<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Entity\Repository;

use Ashenvale\Forestsong\Entity\Investment;
use Ashenvale\Forestsong\ValueObject\InvestmentSearch;
use Pagerfanta\Pagerfanta;

interface InvestmentRepositoryInterface
{
    public function search(InvestmentSearch $search): Pagerfanta;

    public function add(Investment $investment);
}
