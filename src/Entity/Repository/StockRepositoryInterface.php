<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Entity\Repository;

use Ashenvale\Forestsong\Entity\Stock;

interface StockRepositoryInterface
{
    public function find($id): ?Stock;
}
