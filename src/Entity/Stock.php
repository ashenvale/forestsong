<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Entity;

class Stock
{
    private $id;

    private $name;

    private $pinyin;

    public function __construct(string $id, string $name, string $pinyin)
    {
        $this->id = $id;
        $this->name = $name;
        $this->pinyin = $pinyin;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPinyin(): string
    {
        return $this->pinyin;
    }
}
