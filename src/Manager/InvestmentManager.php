<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Manager;

use Ashenvale\Forestsong\Entity\Factory\InvestmentFactory;
use Ashenvale\Forestsong\Entity\Factory\StockFactory;
use Ashenvale\Forestsong\Entity\Repository\InvestmentRepositoryInterface;
use Ashenvale\Forestsong\Entity\Repository\StockRepositoryInterface;
use Ashenvale\Forestsong\ValueObject\Investment;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InvestmentManager
{
    private $investmentRepository;

    private $stockRepository;

    private $investmentFactory;

    private $stockFactory;

    private $validator;

    public function __construct(
        InvestmentRepositoryInterface $investmentRepository,
        StockRepositoryInterface $stockRepository,
        InvestmentFactory $investmentFactory,
        StockFactory $stockFactory,
        ValidatorInterface $validator
    ) {
        $this->investmentRepository = $investmentRepository;
        $this->stockRepository = $stockRepository;
        $this->investmentFactory = $investmentFactory;
        $this->stockFactory = $stockFactory;
        $this->validator = $validator;
    }

    public function new(Investment $investment, string $userId)
    {
        $violations = $this->validator->validate($investment, null, 'stockId');
        if (\count($violations) > 0) {
            throw new ViolationsException($violations);
        }

        $stock = $this->stockRepository->find($investment->getStockId());
        if (null === $stock) {
            $stock = $this->stockFactory->create($investment);
        }

        $entity = $this->investmentFactory->create($investment, $userId, $stock);
        $this->investmentRepository->add($entity);
    }
}
