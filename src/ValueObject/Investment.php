<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\ValueObject;

use Symfony\Component\Validator\Constraints as Assert;

class Investment
{
    /**
     * @Assert\NotBlank(groups={"stockId", "stock"})
     * @Assert\Regex("\d{6}")
     */
    private $stockId;

    /**
     * @Assert\NotBlank(groups={"stock"})
     * @Assert\Regex("[*\p{Han}\w]", groups={"stock"})
     */
    private $stockName;

    /**
     * @Assert\NotBlank(groups={"stock"})
     * @Assert\Regex("\w+", groups={"stock"})
     */
    private $stockPinyin;

    /**
     * @Assert\GreaterThan(0, groups={"investment"})
     */
    private $quantity;

    /**
     * @Assert\GreaterThan(0, groups={"investment"})
     */
    private $price;

    /**
     * @Assert\LessThanOrEqual("now", groups={"investment"})
     */
    private $investedAt;

    public function __construct(
        string $stockId,
        int $quantity,
        float $price,
        \DateTime $investedAt,
        string $stockName = null,
        string $stockPinyin = null
    ) {
        $this->stockId = $stockId;
        $this->stockName = $stockName;
        $this->stockPinyin = $stockPinyin;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->investedAt = $investedAt;
    }

    public function getStockId(): string
    {
        return $this->stockId;
    }

    public function getStockName(): string
    {
        return $this->stockName;
    }

    public function getStockPinyin(): string
    {
        return $this->getStockPinyin;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getInvestedAt(): \DateTime
    {
        return $this->investedAt;
    }
}
