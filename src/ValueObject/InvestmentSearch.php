<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\ValueObject;

class InvestmentSearch
{
    private $keyword;

    private $desc;

    private $months;

    public function __construct(string $keyword = null, bool $desc = false, int $months = 3)
    {
        $this->keyword = $keyword;
        $this->desc = $desc;
        $this->months = $months;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function isDesc(): bool
    {
        return $this->desc;
    }

    public function getMonths(): int
    {
        return $this->months;
    }
}
