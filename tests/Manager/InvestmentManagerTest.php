<?php

/*
 * This file is part of the Ashenvale Forestsong package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Forestsong\Test\Manager;

use Ashenvale\Forestsong\Entity\Factory\InvestmentFactory;
use Ashenvale\Forestsong\Entity\Factory\StockFactory;
use Ashenvale\Forestsong\Entity\Investment as InvestmentEntity;
use Ashenvale\Forestsong\Entity\Repository\InvestmentRepositoryInterface;
use Ashenvale\Forestsong\Entity\Repository\StockRepositoryInterface;
use Ashenvale\Forestsong\Entity\Stock as StockEntity;
use Ashenvale\Forestsong\Manager\InvestmentManager;
use Ashenvale\Forestsong\ValueObject\Investment as InvestmentVo;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InvestmentManagerTest extends TestCase
{
    public function testNew()
    {
        $stockId = '000002';
        $stockName = '万科';
        $stockPinyin = 'wanke';
        $quantity = 100;
        $price = 21.56;
        $investedAt = new \DateTime('yesterday 10:00');
        $userId = 1;

        $vo = new InvestmentVo($stockId, $quantity, $price, $investedAt, $stockName, $stockPinyin);
        $stockEntity = new StockEntity($stockId, $stockName, $stockPinyin);
        $entity = new InvestmentEntity($userId, $stockEntity, $quantity, $price, $investedAt);

        $stockRepo = $this->prophesize(StockRepositoryInterface::class);

        $investmentRepo = $this->prophesize(InvestmentRepositoryInterface::class);
        $investmentRepo->add($entity)->shouldBeCalled();

        $stockFactory = $this->prophesize(StockFactory::class);
        $stockFactory->create($vo)->shouldBeCalled()->willReturn($stockEntity);

        $investmentFactory = $this->prophesize(InvestmentFactory::class);
        $investmentFactory->create($vo, $userId, $stockEntity)->shouldBeCalled()->willReturn($entity);

        $violations = $this->prophesize(ConstraintViolationListInterface::class);
        $violations->count()->shouldBeCalled()->willReturn(0);

        $validator = $this->prophesize(ValidatorInterface::class);
        $validator->validate($vo, null, 'stockId')->shouldBeCalled()->willReturn($violations->reveal());

        $manager = new InvestmentManager(
            $investmentRepo->reveal(),
            $stockRepo->reveal(),
            $investmentFactory->reveal(),
            $stockFactory->reveal(),
            $validator->reveal()
        );
        $manager->new($vo, $userId);
    }
}
